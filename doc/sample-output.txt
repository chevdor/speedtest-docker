Init cron task with '* * * * * root /usr/bin/nice -n -20 /usr/local/bin/run.sh' in '/etc/cron.d/speedtest-cron'
Speedtest by Ookla 1.0.0.2 (5ae238b) Linux/x86_64-linux-musl 4.4.59+ x86_64

The official command line client for testing the speed and performance
of your internet connection.
Closest servers:

    ID  Name                           Location             Country
==============================================================================
 24059  Vialis                         Colmar               France
 15874  IWB Telekom                    Basel                Switzerland
 11769  Vialis                         Strasbourg           France
 29543  ORANGE FRANCE                  Strasbourg           France
 16497  green.ch AG                    Lupfig               Switzerland
 27542  sasag Kabelkommunikation AG    Schaffhausen         Switzerland
  5155  FSIT AG                        Dietikon             Switzerland
 31102  GIB-Solutions AG               Uitikon Waldegg      Switzerland
 15728  Monzoon Networks AG            Zurich               Switzerland
  6251  nexellent AG                   Zurich               Switzerland
{"results":[{"statement_id":0}]}
Init finished, the first test will run according to your cron task: /etc/cron.d/speedtest-cron:
* * * * * root /usr/local/bin/run.sh >> /var/log/cron.log 2>&1
The data will be sent to 192.168.0.123:8086 in database speedtests...
Using any server
Running speedtest. It takes a few seconds, please wait...
==============================================================================

You may only use this Speedtest software and information generated
from it for personal, non-commercial use, through a command line
interface on a personal computer. Your use of this software is subject
to the End User License Agreement, Terms of Use and Privacy Policy at
these URLs:

	https://www.speedtest.net/about/eula
	https://www.speedtest.net/about/terms
	https://www.speedtest.net/about/privacy

==============================================================================

License acceptance recorded. Continuing.

==============================================================================

Ookla collects certain data through Speedtest that may be considered
personally identifiable, such as your IP address, unique device
identifiers or location. Ookla believes it has a legitimate interest
to share this data with internet providers, hardware manufacturers and
industry regulators to help them understand and create a better and
faster internet. For further information including how the data may be
shared, where the data may be transferred and Ookla's contact details,
please see our Privacy Policy at:

       http://www.speedtest.net/privacy

==============================================================================

License acceptance recorded. Continuing.

Parsing data...
▼53448929 ▲2468371
29543: France/Strasbourg/ORANGE ▼427.59 Mbps  ▲19.74 Mbps  ◉14.937 ms
Sending data to influxdb
Done
The data will be sent to 192.168.0.123:8086 in database speedtests...
Using any server
Running speedtest. It takes a few seconds, please wait...
Parsing data...
▼52487429 ▲2474459
29543: France/Strasbourg/ORANGE ▼419.89 Mbps  ▲19.79 Mbps  ◉14.765 ms
Sending data to influxdb
Done
The data will be sent to 192.168.0.123:8086 in database speedtests...
Using any server
Running speedtest. It takes a few seconds, please wait...
Parsing data...
▼51854351 ▲2403268
11769: France/Strasbourg/Vialis ▼414.83 Mbps  ▲19.22 Mbps  ◉23.044 ms
Sending data to influxdb
Done
The data will be sent to 192.168.0.123:8086 in database speedtests...
Using any server
Running speedtest. It takes a few seconds, please wait...
Parsing data...
▼52765700 ▲2249476
29543: France/Strasbourg/ORANGE ▼422.12 Mbps  ▲17.99 Mbps  ◉14.848 ms
Sending data to influxdb
Done
The data will be sent to 192.168.0.123:8086 in database speedtests...
Using any server
Running speedtest. It takes a few seconds, please wait...
Parsing data...
▼51921763 ▲2421418
16497: Switzerland/Lupfig/green.ch ▼415.37 Mbps  ▲19.37 Mbps  ◉18.318 ms
Sending data to influxdb
Done
The data will be sent to 192.168.0.123:8086 in database speedtests...
Using any server
Running speedtest. It takes a few seconds, please wait...
Parsing data...
▼52282924 ▲2481481
29543: France/Strasbourg/ORANGE ▼418.26 Mbps  ▲19.85 Mbps  ◉14.752 ms
Sending data to influxdb
Done
The data will be sent to 192.168.0.123:8086 in database speedtests...
Using any server
Running speedtest. It takes a few seconds, please wait...
Parsing data...
▼52475963 ▲2463273
6251: Switzerland/Zurich/nexellent ▼419.80 Mbps  ▲19.70 Mbps  ◉18.391 ms
Sending data to influxdb
Done
The data will be sent to 192.168.0.123:8086 in database speedtests...
Using any server
Running speedtest. It takes a few seconds, please wait...
Parsing data...
▼50629560 ▲1758331
29543: France/Strasbourg/ORANGE ▼405.03 Mbps  ▲14.06 Mbps  ◉113.79 ms
Sending data to influxdb
Done
The data will be sent to 192.168.0.123:8086 in database speedtests...
Using any server
Running speedtest. It takes a few seconds, please wait...
Parsing data...
▼52976431 ▲2458832
29543: France/Strasbourg/ORANGE ▼423.81 Mbps  ▲19.67 Mbps  ◉14.341 ms
Sending data to influxdb
Done
The data will be sent to 192.168.0.123:8086 in database speedtests...
Using any server
Running speedtest. It takes a few seconds, please wait...
Parsing data...
▼51968880 ▲2114551
6251: Switzerland/Zurich/nexellent ▼415.75 Mbps  ▲16.91 Mbps  ◉17.083 ms
Sending data to influxdb
Done
The data will be sent to 192.168.0.123:8086 in database speedtests...
Using any server
Running speedtest. It takes a few seconds, please wait...
Parsing data...
▼53014730 ▲2458156
29543: France/Strasbourg/ORANGE ▼424.11 Mbps  ▲19.66 Mbps  ◉14.796 ms
Sending data to influxdb
Done
The data will be sent to 192.168.0.123:8086 in database speedtests...
Using any server
Running speedtest. It takes a few seconds, please wait...
Parsing data...
▼52820017 ▲2477288
29543: France/Strasbourg/ORANGE ▼422.56 Mbps  ▲19.81 Mbps  ◉15.498 ms
Sending data to influxdb
Done
The data will be sent to 192.168.0.123:8086 in database speedtests...
Using any server
Running speedtest. It takes a few seconds, please wait...
Parsing data...
▼51450181 ▲2409713
24059: France/Colmar/Vialis ▼411.60 Mbps  ▲19.27 Mbps  ◉22.763 ms
Sending data to influxdb
Done
The data will be sent to 192.168.0.123:8086 in database speedtests...
Using any server
Running speedtest. It takes a few seconds, please wait...
Parsing data...
▼51727132 ▲2411952
6251: Switzerland/Zurich/nexellent ▼413.81 Mbps  ▲19.29 Mbps  ◉17.382 ms
Sending data to influxdb
Done
The data will be sent to 192.168.0.123:8086 in database speedtests...
Using any server
Running speedtest. It takes a few seconds, please wait...
Parsing data...
▼53214450 ▲2474024
29543: France/Strasbourg/ORANGE ▼425.71 Mbps  ▲19.79 Mbps  ◉15.608 ms
Sending data to influxdb
Done
The data will be sent to 192.168.0.123:8086 in database speedtests...
Using any server
Running speedtest. It takes a few seconds, please wait...
Parsing data...
▼53367120 ▲2478187
29543: France/Strasbourg/ORANGE ▼426.93 Mbps  ▲19.82 Mbps  ◉15.302 ms
Sending data to influxdb
Done
The data will be sent to 192.168.0.123:8086 in database speedtests...
Using any server
Running speedtest. It takes a few seconds, please wait...
Parsing data...
▼51495451 ▲2402829
31102: Switzerland/Uitikon/GIB-Solutions ▼411.96 Mbps  ▲19.22 Mbps  ◉20.656 ms
Sending data to influxdb
Done
The data will be sent to 192.168.0.123:8086 in database speedtests...
Using any server
Running speedtest. It takes a few seconds, please wait...
Parsing data...
▼53179122 ▲2470648
29543: France/Strasbourg/ORANGE ▼425.43 Mbps  ▲19.76 Mbps  ◉14.782 ms
Sending data to influxdb
Done
The data will be sent to 192.168.0.123:8086 in database speedtests...
Using any server
Running speedtest. It takes a few seconds, please wait...
Parsing data...
▼52704722 ▲2461601
29543: France/Strasbourg/ORANGE ▼421.63 Mbps  ▲19.69 Mbps  ◉15.529 ms
Sending data to influxdb
Done
The data will be sent to 192.168.0.123:8086 in database speedtests...
Using any server
Running speedtest. It takes a few seconds, please wait...
Parsing data...
▼52164778 ▲2442458
6251: Switzerland/Zurich/nexellent ▼417.31 Mbps  ▲19.53 Mbps  ◉17.145 ms
Sending data to influxdb
Done
The data will be sent to 192.168.0.123:8086 in database speedtests...
Using any server
Running speedtest. It takes a few seconds, please wait...
Parsing data...
▼51948583 ▲2446088
6251: Switzerland/Zurich/nexellent ▼415.58 Mbps  ▲19.56 Mbps  ◉16.997 ms
Sending data to influxdb
Done
The data will be sent to 192.168.0.123:8086 in database speedtests...
Using any server
Running speedtest. It takes a few seconds, please wait...
Parsing data...
▼52726459 ▲2442444
29543: France/Strasbourg/ORANGE ▼421.81 Mbps  ▲19.53 Mbps  ◉14.842 ms
Sending data to influxdb
Done
The data will be sent to 192.168.0.123:8086 in database speedtests...
Using any server
Running speedtest. It takes a few seconds, please wait...
Parsing data...
▼51688178 ▲2426320
27542: Switzerland/Schaffhausen/sasag ▼413.50 Mbps  ▲19.41 Mbps  ◉18.933 ms
Sending data to influxdb
Done,
